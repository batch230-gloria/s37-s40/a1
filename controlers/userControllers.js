const User = require("../models/user.js");
const Course = require("../models/course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10),
	
	})

	return newUser.save().then((user, error)=>{

		if(error){
			return false;
		}
		else{
			return true;
		}

	})	
}


// function checkEmailExist{}
module.exports.checkEmailExist = (reqBody) =>{
	return User.find({email: reqBody.email}).then(result =>{

		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			// compareSync is a bcrypt function to compare unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); // return true or false
			if(isPasswordCorrect){
				// Let's give the user a token to a access features
				return{access: auth.createAccessToken(result)};
			}
			else{
				// If password does not match, else
				return false;
			}
		}
	})
}


// [ACTIVITY - s38]

/*module.exports.getProfile = (reqBody) => {

	return User.findById({_id: reqBody.id}).then((result,err) => {

		if(err){
			return false;
		}
		else{
			result.password = "******";
			return result.save()
		}
		
	})

}*/

// s38 version 2 (retrieving user detail using token)
module.exports.getProfile = (request, response) => {

	// will contain your decoded token
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result => {
		result.password = "******";
		response.send(result);
	})
}











// enroll feature
module.exports.enroll = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	let courseName = await Course.findById(request.body.courseId).then(result => result.name);

	let newData = {
		// User ID and email will be retrieved from the request header(request header contains the user token)
		userId: userData.id,
		email: userData.email,
		// Course ID will be retrieved from the request body
		courseId: request.body.courseId,
		courseName: courseName
	}

	console.log(newData);

	// a user document is updated if we received a "true" value
	let isUserUpdated = await User.findById(newData.userId)

	.then(user =>{
		user.enrollments.push({
			courseId: newData.courseId,
			courseName: newData.courseName
		})
		// Save the updated user information from the database
		return user.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated);

	let isCourseUpdated = await Course.findById(newData.courseId)
	.then(result =>{
			if(result.slots <= 0){
				console.log('Slot is already zero')
			}
		})
	return result
	
	.then(course => {

		course.enrollees.push({
			userId: newData.userId,
			email: newData.email			
		})


		// Mini Activity -
		// [1] Create a condition that if the slots is already zero, no deduction of slot will happen and...
		// [2] it should have a message in the terminal that the slot is already zero
		// [3] Else if the slot is negative value, it should make the slot value to zero
		

		// Minus the slots available by 1
		// course.slots = course.slots - 1;
		course.slots -= 1
		

		return course.save()

		.then(result =>{
			console.log(result);
			return (true);
		})
		.catch(error=>{
			console.log(error);
			return false;
		})
	})

	console.log(isCourseUpdated);

	// Ternary operator
	(isUserUpdated == true && isCourseUpdated == true)? response.send(true) : response.send(false);
	/*
		if(isUserUpdated == true && isCourseUpdated == true){
			response.send(true);
		}
		else{
			response.send(false);
		}
	*/

}
